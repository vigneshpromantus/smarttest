package driverBase.windowsHandling;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class WindowsHandling {
	public static void windowhandling() {
		JSONObject json = new JSONObject();
		long starttime = 0L;
		long endtime = 0L;
		LocalDateTime startdate = null;
		String duration = null;
		LocalDateTime enddate = null;

		try {

			starttime = System.currentTimeMillis();
			startdate = LocalDateTime.now();

			Set<String> windowHandles = SeleniumStart.driver.getWindowHandles();
			List<String> windowStrings = new ArrayList<>(windowHandles);
			String reqWindow = windowStrings.get(SeleniumStart.viewtestcaseresponsejson.optInt("windowIndex"));
			SeleniumStart.driver.switchTo().window(reqWindow);

			enddate = LocalDateTime.now();
			endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", TimeInitialization.dtf.format(startdate));
			json.put("endtime", TimeInitialization.dtf.format(enddate));
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);
		} catch (Exception e) {
			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}

			System.err.println("+++" + e + "++++");
		 }

	}
}
