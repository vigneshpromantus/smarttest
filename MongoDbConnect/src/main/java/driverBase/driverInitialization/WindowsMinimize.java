package driverBase.driverInitialization;

import org.json.JSONObject;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class WindowsMinimize {

	public static void windowsMinimize() {

		long starttime = 0L;
		String duration = null;
		JSONObject json = new JSONObject();
		try {

			starttime = System.currentTimeMillis();
			
			SeleniumStart.driver.manage().window().minimize();
			
			long endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);

		} catch (Exception e) {

			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}

		}

	}
}
