package driverBase.driverInitialization;

import org.json.JSONObject;
import org.openqa.selenium.Point;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class GetPosition {
	public static void getPosition() {
		long starttime = 0L;
		String duration = null;
		JSONObject json = new JSONObject();

		try {

			starttime = System.currentTimeMillis();

			Point position = SeleniumStart.driver.manage().window().getPosition();

			long endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			json.put("Position", position);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);

		} catch (Exception e) {

			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}


		}
	}
}
