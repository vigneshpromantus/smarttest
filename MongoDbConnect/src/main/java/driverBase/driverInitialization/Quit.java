package driverBase.driverInitialization;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.openqa.selenium.WebElement;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class Quit {
	public static void quit() {

		JSONObject json = new JSONObject();
		long starttime = 0L;
		long endtime = 0L;
		String duration = null;
		WebElement element = null;

		try {
			starttime = System.currentTimeMillis();

			SeleniumStart.driver.quit();

			endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime",starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);
		} catch (Exception e) {
			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}


			System.err.println("+++" + e + "++++");
		}

	}
}
