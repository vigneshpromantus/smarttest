package driverBase.Action;

import java.time.Duration;
import java.time.LocalDateTime;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class ReleaseWithArg {
	public static Actions actions = new Actions(SeleniumStart.driver);

	public static void releaseWithArg() {

		JSONObject json = new JSONObject();
		long starttime = 0L;
		long endtime = 0L;
		String duration = null;
		WebElement element = null;
		try {

			starttime = System.currentTimeMillis();

			WebDriverWait wait = new WebDriverWait(SeleniumStart.driver, Duration.ofSeconds(10));

			switch ((SeleniumStart.viewtestcaseresponsejson.optString("locatorType"))) {

			case "xpath":
				element = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("xpathValue"))));

				break;
			case "id":

				element = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("idValue"))));
				break;

			case "name":
				element = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("nameValue"))));

				break;

			case "className":

				element = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("classNameValue"))));
				break;

			case "cssSelector":

				element = wait.until(ExpectedConditions
						.elementToBeClickable(By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("CSSValue"))));
				break;

			case "tagName":

				element = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("tagNameValue"))));
				break;

			case "linkText":

				element = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("linkTextValue"))));
				break;

			case "partiallinkText":

				element = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("partialLsinkTextValue"))));
				break;

			}

			actions.release(element).build().perform();

			endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);

		} catch (Exception e) {

			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}

			System.err.println("+++" + e + "++++");

		}
	}
}
