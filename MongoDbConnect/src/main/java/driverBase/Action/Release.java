package driverBase.Action;

import java.time.LocalDateTime;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class Release {
	public static Actions actions = new Actions(SeleniumStart.driver);

	public static void release() {

		JSONObject json = new JSONObject();
		long starttime = 0L;
		long endtime = 0L;
		String duration = null;
		WebElement element = null;
		try {

			starttime = System.currentTimeMillis();

			// WebDriverWait wait = new WebDriverWait(SeleniumStart.driver,
			// Duration.ofSeconds(10));
//			WebElement findElement = SeleniumStart.driver
//					.findElement(By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("xpathValue")));
//			actions.keyDown(findElement, duration);

			actions.release().build().perform();

			endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);

		} catch (Exception e) {

			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}

			System.err.println("+++" + e + "++++");

		}
	}
}
