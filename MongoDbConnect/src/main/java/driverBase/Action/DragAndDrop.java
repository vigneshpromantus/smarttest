package driverBase.Action;

import java.time.Duration;
import java.time.LocalDateTime;

import org.json.JSONObject;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.mongoDb.connection.baseClass.TimeInitialization;
import com.mongoDb.connection.selenium.SeleniumStart;

public class DragAndDrop {
	public static Actions actions = new Actions(SeleniumStart.driver);

	public static void dragAndDrop() {

		JSONObject json = new JSONObject();
		long starttime = 0L;
		long endtime = 0L;
		String duration = null;
		WebElement element1 = null, element2 = null;
		try {

			starttime = System.currentTimeMillis();

			WebDriverWait wait = new WebDriverWait(SeleniumStart.driver, Duration.ofSeconds(10));

			switch ((SeleniumStart.viewtestcaseresponsejson.optString("locatorType"))) {

			case "xpath":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("xpathValue1"))));

				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.xpath(SeleniumStart.viewtestcaseresponsejson.optString("xpathValue2"))));

				break;

			case "id":
				element1 = wait.until(ExpectedConditions
						.elementToBeClickable(By.id(SeleniumStart.viewtestcaseresponsejson.optString("idValue1"))));

				element2 = wait.until(ExpectedConditions
						.elementToBeClickable(By.id(SeleniumStart.viewtestcaseresponsejson.optString("idValue2"))));
				break;

			case "name":
				element1 = wait.until(ExpectedConditions
						.elementToBeClickable(By.name(SeleniumStart.viewtestcaseresponsejson.optString("nameValue1"))));
				element2 = wait.until(ExpectedConditions
						.elementToBeClickable(By.name(SeleniumStart.viewtestcaseresponsejson.optString("nameValue2"))));

				break;

			case "className":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.className(SeleniumStart.viewtestcaseresponsejson.optString("classNameValue1"))));
				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.className(SeleniumStart.viewtestcaseresponsejson.optString("classNameValue2"))));
				break;

			case "cssSelector":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.cssSelector(SeleniumStart.viewtestcaseresponsejson.optString("CSSValue1"))));
				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.cssSelector(SeleniumStart.viewtestcaseresponsejson.optString("CSSValue2"))));

				break;

			case "tagName":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.tagName(SeleniumStart.viewtestcaseresponsejson.optString("tagNameValue1"))));
				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.tagName(SeleniumStart.viewtestcaseresponsejson.optString("tagNameValue2"))));

				break;

			case "linkText":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.linkText(SeleniumStart.viewtestcaseresponsejson.optString("linkTextValue1"))));

				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.linkText(SeleniumStart.viewtestcaseresponsejson.optString("linkTextValue2"))));
				break;

			case "partiallinkText":
				element1 = wait.until(ExpectedConditions.elementToBeClickable(
						By.partialLinkText(SeleniumStart.viewtestcaseresponsejson.optString("partialLinkTextValue1"))));
				element2 = wait.until(ExpectedConditions.elementToBeClickable(
						By.partialLinkText(SeleniumStart.viewtestcaseresponsejson.optString("partialLinkTextValue2"))));
				break;
			}

			actions.dragAndDrop(element1, element2).build().perform();

			endtime = System.currentTimeMillis();
			duration = TimeInitialization.format.format((endtime - starttime) / 1000d);
			json.put("starttime", starttime);
			json.put("endtime", endtime);
			json.put("status", "Success");
			json.put("duration", duration);
			SeleniumStart.viewtestcaseresponsejson.put("result", json);

		} catch (Exception e) {

			if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("true")) {
				SeleniumStart.driver.quit();
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
			} else if (SeleniumStart.viewtestcaseresponsejson.optString("blocker").equals("false")) {
				json.put("starttime", starttime);
				json.put("status", "Failure");
				json.put("exceptionoccured", e.getMessage());
				SeleniumStart.viewtestcaseresponsejson.put("result", json);
			}


			System.err.println("+++" + e + "++++");

		}
	}
}
