package com.mongoDb.connection.creatingapi;

import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongoDb.connection.mongodata.ResponceFunctionPojo;
import com.mongoDb.connection.mongoimplementation.ProjectImpl;
import com.mongoDb.connection.mongoimplementation.ResponceFunctionImpl;
import com.mongoDb.connection.mongoimplementation.TestCaseImpl;

@RestController
@RequestMapping("/platformtesting")
public class CreatingApi {

	public WebDriver driver;
	@Autowired
	ProjectImpl projectimplemantation;

	@Autowired
	TestCaseImpl testcaseimplementation;

	@Autowired
	ResponceFunctionImpl Responceimplementation;

	@GetMapping("/{userid}")
	public Map<String, String> getProjectResponse(@PathVariable String userid)
			throws IOException, InterruptedException {
		Map<String, String> responsemessage = new HashMap<>();
		boolean empty = projectimplemantation.findById(userid).isEmpty();

		if (empty) {

			responsemessage.put("status", "failure");
			responsemessage.put("message", "Does not contain project for this given project id...");
			responsemessage.put("Chrome", "No active sessions for this credit");
			return responsemessage;

		} else {
			
			ProcessBuilder build1 = new ProcessBuilder("java", "-jar",
					"/home/promantus/Documents/workspace-spring-tool-suite-4-4.17.0.RELEASE/MongoDbConnect/src/main/java/com/mongoDb/connection/creatingapi/NewTesting6.jar",
					userid);
			build1.redirectOutput(Redirect.INHERIT);
			build1.redirectError(Redirect.INHERIT);

			Process start = build1.start();
			
			long pid = start.pid();
			
			String valueOf = String.valueOf(pid);

			ProcessBuilder build2 = new ProcessBuilder("java", "-jar",
					"/home/promantus/Documents/workspace-spring-tool-suite-4-4.17.0.RELEASE/MongoDbConnect/src/main/java/com/mongoDb/connection/creatingapi/KillProcess1.jar",
					userid, valueOf);
			build2.redirectOutput(Redirect.INHERIT);
			build2.redirectError(Redirect.INHERIT);
			build2.start();

			ResponseEntity<List<ResponceFunctionPojo>> responsemessage1 = ResponseEntity
					.ok(Responceimplementation.findBy(userid));
			JSONObject json = new JSONObject();
			json.put("responsedata", responsemessage1.getBody());
			JSONArray json1 = json.getJSONArray("responsedata");
			for (int i = 0; i < json1.length(); i++) {

				JSONObject json2 = json1.getJSONObject(i);
				if (json2.optString("webdriverstatus").equals("Connected")) {

					responsemessage.put("status", "success");
					responsemessage.put("message", "started selenium session for this project id");
					responsemessage.put("chrome", "Session Active");

				} else {

					responsemessage.put("status", "success");
					responsemessage.put("message", "already selenium session has running");
					responsemessage.put("chrome", "Session is already active");
				}

			}

			return responsemessage;
		}
	}
}






