package com.mongoDb.connection.apiMethods;

import org.json.JSONObject;

import com.mongoDb.connection.baseClass.GetConfig;

import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.response.Response;

public class ApiMethods {
	public static Response findApi(JSONObject data) {
		Response response = null;
		try {
			response = ApiServices.findApi(Headers.headers(new Header("token", GetConfig.getConfigure("token"))), data);
		} catch (Exception e) {

		}
		return response;
	}

	public static Response insertApi(JSONObject data) {
		Response response = null;
		try {
			response = ApiServices.insertApi(Headers.headers(new Header("token", GetConfig.getConfigure("token"))),
					data);

		} catch (Exception e) {

		}
		return response;
	}

	public static Response updateApi(JSONObject data) {
		Response response = null;
		try {
			response = ApiServices.updateApi(Headers.headers(new Header("token", GetConfig.getConfigure("token"))),
					data);
		} catch (Exception e) {

		}
		return response;
	}

}
