package com.mongoDb.connection.apiMethods;

import org.json.JSONObject;

import io.restassured.RestAssured;
import io.restassured.http.Headers;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import static io.restassured.RestAssured.*;

public class ApiServices {
	public static String baseurl = "http://localhost:9199/platformtesting";

	public static Response post(String posturl, Headers headers, JSONObject data) {
		Response post = null;
		try {
			
			baseURI = baseurl;
			RequestSpecification given = RestAssured.given();
			given.header("Content-Type", "application/json");
			if (data != null && headers != null) {
				post = given.body(data.toString()).headers(headers).post(posturl);
			}

		} catch (Exception e) {

		}
		return post;
	}

	public static Response findApi(Headers headers, JSONObject data) {
		Response response = null;
		try {
			response = post("/mongo/find", headers, data);
		} catch (Exception e) {

		}
		return response;
	}

	public static Response insertApi(Headers headers, JSONObject data) {
		Response response = null;
		try {
			response = post("/mongo/insert", headers, data);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

	public static Response updateApi(Headers headers, JSONObject data) {
		Response response = null;
		try {
			response = post("/mongo/update", headers, data);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return response;
	}

}
