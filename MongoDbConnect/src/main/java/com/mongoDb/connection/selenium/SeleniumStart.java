package com.mongoDb.connection.selenium;

import org.json.JSONArray;
import org.json.JSONObject;
import org.openqa.selenium.WebDriver;

import com.mongoDb.connection.apiMethods.ApiMethods;

public class SeleniumStart {
	public static WebDriver driver;
	public static JSONObject viewtestcaseresponsejson;
	public static JSONObject result = new JSONObject();
	public static JSONArray testcasejsonarray;
	public static JSONObject insertdata;

	public static void seleniumTriggering(String projectId) throws InterruptedException {

		JSONObject viewprojectouterjson = new JSONObject();
		viewprojectouterjson.put("collection", "project");
		JSONObject viewprojectinnerjson = new JSONObject();
		viewprojectinnerjson.put("projectId", projectId);
		viewprojectouterjson.put("query", viewprojectinnerjson);
		String viewprojectresponse = ApiMethods.findApi(viewprojectouterjson).asPrettyString();

		JSONObject viewcomponentouterjson = new JSONObject();
		viewcomponentouterjson.put("collection", "component");
		JSONObject viewcomponentinnerjson = new JSONObject();
		viewcomponentinnerjson.put("projectId", projectId);
		viewcomponentouterjson.put("query", viewcomponentinnerjson);
		String viewcomponentresponse = ApiMethods.findApi(viewcomponentouterjson).asPrettyString();

		JSONObject viewmoduleouterjson = new JSONObject();
		viewmoduleouterjson.put("collection", "module");
		JSONObject viewmoduleinnerjson = new JSONObject();
		viewmoduleinnerjson.put("projectId", projectId);
		viewmoduleouterjson.put("query", viewmoduleinnerjson);
		String viewmoduleresponse = ApiMethods.findApi(viewmoduleouterjson).asPrettyString();

		JSONObject viewscenarioouterjson = new JSONObject();
		viewscenarioouterjson.put("collection", "scenarios");
		JSONObject viewscenarioinnerjson = new JSONObject();
		viewscenarioinnerjson.put("projectId", projectId);
		viewscenarioouterjson.put("query", viewscenarioinnerjson);
		String viewscenarioresponse = ApiMethods.findApi(viewscenarioouterjson).asPrettyString();

		JSONObject viewtestcaseouterjson = new JSONObject();
		viewtestcaseouterjson.put("collection", "testcase");
		JSONObject viewtestcaseinnerjson = new JSONObject();
		viewtestcaseinnerjson.put("projectId", projectId);
		viewtestcaseouterjson.put("query", viewtestcaseinnerjson);
		String viewtestcaseresponse = ApiMethods.findApi(viewtestcaseouterjson).asPrettyString();
		testcasejsonarray = new JSONArray(viewtestcaseresponse);
		for (int i = 0; i < testcasejsonarray.length(); i++) {

			viewtestcaseresponsejson = testcasejsonarray.getJSONObject(i);
			if (viewtestcaseresponsejson.optString("actionType").equals("Location Redirect")) {
				driverBase.driverInitialization.DriverInitialize.initializeDriver();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("feeddata")) {
				driverBase.element.FeedData.feedData();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("clickevent")) {
				driverBase.element.Click.click();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("keydown")) {
				driverBase.Action.KeyDown.keyDown();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("navigate")) {
				driverBase.driverInitialization.Navigate.navigate();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("doubleclick")) {
				driverBase.Action.DoubleClick.doubleclick();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("movetoelement")) {
				driverBase.Action.MoveToElement.moveToElement();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("clickandhold")) {
				driverBase.Action.ClickAndHold.clickAndHold();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("windowhandling")) {
				driverBase.windowsHandling.WindowsHandling.windowhandling();
			} else if (viewtestcaseresponsejson.optString("actionType").equals("windowsmini")) {
				driverBase.driverInitialization.WindowsMinimize.windowsMinimize();
			}

		}

		insertdata = new JSONObject();
		insertdata.put("doc", testcasejsonarray);

		insertdata.put("collection", "results");

		ApiMethods.insertApi(insertdata);

	}

	public static void main(String[] args) throws InterruptedException {
		seleniumTriggering(args[0]);

	}

}
