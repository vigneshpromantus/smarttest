package com.mongoDb.connection.exceptionhandler;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.NoHandlerFoundException;

@ControllerAdvice
public class AppExceptions {

	@ExceptionHandler(value = NoHandlerFoundException.class)
	public ResponseEntity<Map<String, String>> handleNotFoundException(NoHandlerFoundException exception,
			WebRequest webrequest) {
		Map<String, String> responseMessage = new HashMap<>();
		responseMessage.put("Message", "Application Error");
		responseMessage.put("Status", "Failure");
		return new ResponseEntity<Map<String, String>>(responseMessage, HttpStatus.OK);
	}

	@ExceptionHandler(value = MissingRequestHeaderException.class)
	public ResponseEntity<Map<String, String>> missingRequestedHeaderexception() {
		Map<String, String> tokenException = new HashMap<>();
		tokenException.put("Message", "Header Value is Missing");
		tokenException.put("Status", "Failure");

		return new ResponseEntity<Map<String, String>>(tokenException, HttpStatus.OK);

	}

}
