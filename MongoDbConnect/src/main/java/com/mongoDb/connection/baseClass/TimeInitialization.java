package com.mongoDb.connection.baseClass;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.format.DateTimeFormatter;

public class TimeInitialization {
	public static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss:SSS");
	public static NumberFormat format = new DecimalFormat("#0.00");
}
