package com.mongoDb.connection.baseClass;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.json.JSONObject;

public class GetConfig {

	public static String getConfigure(String key) throws IOException {
		String optString = null;
		try {
			String filepath = new String(
					"/home/promantus/Documents/workspace-spring-tool-suite-4-4.17.1.RELEASE/MongoDbConnect/src/main/java/com/config/token.json");
			File file = new File(filepath);
			String string = new String(Files.readAllBytes(Paths.get(file.toURI())));
			JSONObject obj = new JSONObject(string);
			optString = obj.optString(key);
		} catch (Exception e) {
			System.out.println("exception arrises is " + e);
		}
		return optString;

	}

}
