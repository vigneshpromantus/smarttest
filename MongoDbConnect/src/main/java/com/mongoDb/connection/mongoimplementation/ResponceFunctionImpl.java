package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.ResponceFunctionPojo;

public interface ResponceFunctionImpl extends MongoRepository<ResponceFunctionPojo, String> {
	@Query("{'projectId':?0}")
	List<ResponceFunctionPojo> findBy(String projectId);
}
