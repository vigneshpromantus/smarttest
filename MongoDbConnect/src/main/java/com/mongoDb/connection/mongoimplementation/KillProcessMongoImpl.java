package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.KillClassMethodPojo;

public interface KillProcessMongoImpl extends MongoRepository<KillClassMethodPojo, String> {

	@Query("{'projectId':?0}")	
	
	List<KillClassMethodPojo> findBy(String projectId);

	
}
