package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.ComponentPojo;

public interface ComponentImpl extends MongoRepository<ComponentPojo, String> {

	@Query("{$or:[{'projectId' : ?0},{'_id' : ?1}]}")
	List<ComponentPojo> findBy(String projectId, String componentId);

}
