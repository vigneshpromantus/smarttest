package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.ScenarioPojo;

public interface ScenarioImpl extends MongoRepository<ScenarioPojo, String> {

	@Query("{$or:[{'projectId':?0},{'componentId':?1},{'modulesId':?2},{'_id':?3}]}")
	List<ScenarioPojo> findBy(String projectId, String componentId, String modulesId, String scenarioId);

}
