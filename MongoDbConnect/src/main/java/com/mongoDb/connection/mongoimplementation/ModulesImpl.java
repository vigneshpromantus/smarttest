package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.ModulePojo;

public interface ModulesImpl extends MongoRepository<ModulePojo, String> {

	@Query("{$or:[{'projectId':?0},{'componentId':?1},{'_id':?2}]}")

	List<ModulePojo> findBy(String projectId, String componentId, String modulesId);

}
