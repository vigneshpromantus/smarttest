package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.mongoDb.connection.mongodata.TestCasePojo;

public interface TestCaseImpl extends MongoRepository<TestCasePojo, String> {

	@Aggregation(pipeline = {
			"{'$match':{$or:[{'projectId':?0},{'componentId':?1},{'modulesId':?2},{'scenarioId':?3},{'_id':?4}]}}",
			"{'$skip':?5}", "{'$limit':?6}" })
	List<TestCasePojo> findBy(String projectId, String componentId, String moduleId, String scenarioId,
			String testcaseId, int skip, int limit);

  }
