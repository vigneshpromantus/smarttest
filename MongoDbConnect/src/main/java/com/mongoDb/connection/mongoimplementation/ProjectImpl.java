package com.mongoDb.connection.mongoimplementation;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import com.mongoDb.connection.mongodata.ProjectPojo;

public interface ProjectImpl extends MongoRepository<ProjectPojo, String> {

	@Query("{'projectId' :?0}")
	   List<ProjectImpl> findBy(String projectId);

}
