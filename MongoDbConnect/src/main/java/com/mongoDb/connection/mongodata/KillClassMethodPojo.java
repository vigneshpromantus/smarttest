package com.mongoDb.connection.mongodata;

public class KillClassMethodPojo {
	private String projectId;
	private String pid;

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

}
