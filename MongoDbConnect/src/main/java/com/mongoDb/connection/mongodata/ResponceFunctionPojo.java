package com.mongoDb.connection.mongodata;

import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "Response")
public class ResponceFunctionPojo {

	private String _id;
	private String projectId;
	private String webdriverstatus;
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public String getProjectId() {
		return projectId;
	}
	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}
	public String getWebdriverstatus() {
		return webdriverstatus;
	}
	public void setWebdriverstatus(String webdriverstatus) {
		this.webdriverstatus = webdriverstatus;
	}
}