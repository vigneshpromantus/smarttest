package com.mongoDb.connection.mongodata;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "testcase")
public class TestCasePojo {
	@Id
	private String testcaseId;
	private String title;
	private String projectId;
	private String componentId;
	private String modulesId;
	private String scenariosId;
	private String accId;
	private String url;
	private String actionType;
	private String xpathValue;
	private String locatorType;
	private String nameValue;
	private String value;
	private String xpathValue1;
	private String xpathValue2;
	private String windowIndex;
	private String blocker;

	public String getBlocker() {
		return blocker;
	}

	public void setBlocker(String blocker) {
		this.blocker = blocker;
	}

	public String getWindowIndex() {
		return windowIndex;
	}

	public void setWindowIndex(String windowIndex) {
		this.windowIndex = windowIndex;
	}

	public String getXpathValue1() {
		return xpathValue1;
	}

	public void setXpathValue1(String xpathValue1) {
		this.xpathValue1 = xpathValue1;
	}

	public String getXpathValue2() {
		return xpathValue2;
	}

	public void setXpathValue2(String xpathValue2) {
		this.xpathValue2 = xpathValue2;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getNameValue() {
		return nameValue;
	}

	public void setNameValue(String nameValue) {
		this.nameValue = nameValue;
	}

	public String getXpathValue() {
		return xpathValue;
	}

	public void setXpathValue(String xpathValue) {
		this.xpathValue = xpathValue;
	}

	public String getLocatorType() {
		return locatorType;
	}

	public void setLocatorType(String locatorType) {
		this.locatorType = locatorType;
	}

	public String getActionType() {
		return actionType;
	}

	public void setActionType(String actionType) {
		this.actionType = actionType;
	}

	public String getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(String testcaseId) {
		this.testcaseId = testcaseId;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getModulesId() {
		return modulesId;
	}

	public void setModulesId(String modulesId) {
		this.modulesId = modulesId;
	}

	public String getScenariosId() {
		return scenariosId;
	}

	public void setScenariosId(String scenariosId) {
		this.scenariosId = scenariosId;
	}

	public String getAccId() {
		return accId;
	}

	public void setAccId(String accId) {
		this.accId = accId;
	}

	public String getUrl() {
		return url;

	}

	public void setUrl(String url) {
		this.url = url;
	}

}
