package com.mongoDb.connection.mongoApi;

import java.io.IOException;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongoDb.connection.baseClass.GetConfig;
import com.mongoDb.connection.requestAPIData.RequestData;
import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;

@RestController
@RequestMapping("/platformtesting")
public class MongoUpdateAPI {

	@Autowired
	MongoTemplate mongoTemplate;

	@PostMapping("/mongo/update")
	public void updateApi(@RequestBody RequestData data, @RequestHeader("token") String headertoken)
			throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("token", GetConfig.getConfigure("token"));
		if (headertoken.equals(GetConfig.getConfigure("token"))) {
			if (data.getCollection().equals("testcase")) {
				BasicDBObject query = new BasicDBObject();
				query.append("projectId", data.getQuery().getProject());
				query.append("componentId", data.getQuery().getComponent());
				query.append("moduleId", data.getQuery().getModule());
				query.append("scenariosId", data.getQuery().getScenario());
				query.append("_id", new ObjectId(data.getQuery().getTestcase()));

				BasicDBObject updateDoc = new BasicDBObject();
				updateDoc.put("sessionStatus", data.getDoc().getSessionStatus());

				BasicDBObject set = new BasicDBObject();
				set.put("$set", updateDoc);

				MongoCollection<Document> collection = mongoTemplate.getCollection(data.getCollection());
				long modifiedCount = collection.updateOne(query, set).getModifiedCount();
				System.out.println(modifiedCount);
			}

		}
	}

}
