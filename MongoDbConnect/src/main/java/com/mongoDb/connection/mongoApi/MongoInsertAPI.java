package com.mongoDb.connection.mongoApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bson.Document;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongoDb.connection.baseClass.GetConfig;
import com.mongoDb.connection.requestAPIData.InsertQuery1;
import com.mongoDb.connection.requestAPIData.InsertQuery2;
import com.mongodb.client.MongoCollection;

@RestController
@RequestMapping("/platformtesting")
public class MongoInsertAPI {
	@Autowired
	MongoTemplate mongo;

	@PostMapping("/mongo/Insert")
	public ResponseEntity<Map<String, String>> MongoInsert(@RequestBody InsertQuery1 insertQuery,
			@RequestHeader("token") String headertoken) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("token", GetConfig.getConfigure("token"));
		Map<String, String> map = new HashMap<>();

		if (headertoken.equals(GetConfig.getConfigure("token"))) {

			List<InsertQuery2> doc = insertQuery.getDoc();
			JSONObject obj = new JSONObject();
			obj.put("Data", doc);

			Document document = Document.parse(obj.toString());
			MongoCollection<Document> collection = mongo.getCollection(insertQuery.getCollection());
			collection.insertOne(document);

		} else if (headertoken.length() <= 0) {

			map.put("Status", "Failure");
			map.put("message", "token can't be blanked");
			return new ResponseEntity<>(map, HttpStatus.OK);

		} else {

			map.put("Status", "Failure");
			map.put("message", "token is incorrect");
			return new ResponseEntity<>(map, HttpStatus.OK);

		}
		return new ResponseEntity<>(map, HttpStatus.OK);

	}

}
