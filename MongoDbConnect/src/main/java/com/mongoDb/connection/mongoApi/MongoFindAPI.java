package com.mongoDb.connection.mongoApi;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongoDb.connection.baseClass.GetConfig;
import com.mongoDb.connection.mongodata.ComponentPojo;
import com.mongoDb.connection.mongodata.ModulePojo;
import com.mongoDb.connection.mongodata.ProjectPojo;
import com.mongoDb.connection.mongodata.ScenarioPojo;
import com.mongoDb.connection.mongodata.TestCasePojo;
import com.mongoDb.connection.mongoimplementation.ComponentImpl;
import com.mongoDb.connection.mongoimplementation.ModulesImpl;
import com.mongoDb.connection.mongoimplementation.ProjectImpl;
import com.mongoDb.connection.mongoimplementation.ScenarioImpl;
import com.mongoDb.connection.mongoimplementation.TestCaseImpl;
import com.mongoDb.connection.requestAPIData.RequestData;

@RestController
@RequestMapping("platformtesting")
public class MongoFindAPI {

	@Autowired
	ProjectImpl project;
	@Autowired
	ComponentImpl component;
	@Autowired
	ScenarioImpl scenario;
	@Autowired
	ModulesImpl modules;
	@Autowired
	TestCaseImpl testcase;

	@PostMapping("/mongo/find")
	public ResponseEntity<Object> getProjectResponse(@RequestBody RequestData data,
			@RequestHeader("token") String headertoken) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("token", GetConfig.getConfigure("token"));
		Map<String, String> map = new HashMap<>();

		if (headertoken.equals(GetConfig.getConfigure("token"))) {
			if (data.getLimit() == 0) {
				data.setLimit(30);
			}

			if (data.getCollection().equals("project")) {
				ResponseEntity<Optional<ProjectPojo>> projectResponce = ResponseEntity
						.ok(project.findById(data.getQuery().getProject()));

				return new ResponseEntity<Object>(projectResponce.getBody(), HttpStatus.OK);

			} else if (data.getCollection().equals("component")) {

				ResponseEntity<List<ComponentPojo>> componentResponse = ResponseEntity
						.ok(component.findBy(data.getQuery().getProject(), data.getQuery().getComponent()));
				return new ResponseEntity<Object>(componentResponse.getBody(), HttpStatus.OK);

			} else if (data.getCollection().equals("module")) {
				ResponseEntity<List<ModulePojo>> modulesResponse = ResponseEntity.ok(modules.findBy(
						data.getQuery().getProject(), data.getQuery().getComponent(), data.getQuery().getModule()));

				return new ResponseEntity<Object>(modulesResponse.getBody(), HttpStatus.OK);

			} else if (data.getCollection().equals("scenarios")) {
				ResponseEntity<List<ScenarioPojo>> scenarioResponse = ResponseEntity
						.ok(scenario.findBy(data.getQuery().getProject(), data.getQuery().getComponent(),
								data.getQuery().getModule(), data.getQuery().getScenario()));

				return new ResponseEntity<Object>(scenarioResponse.getBody(), HttpStatus.OK);

			} else if (data.getCollection().equals("testcase")) {

				ResponseEntity<List<TestCasePojo>> testcaseResponse = ResponseEntity.ok(testcase.findBy(
						data.getQuery().getProject(), data.getQuery().getComponent(), data.getQuery().getModule(),
						data.getQuery().getScenario(), data.getQuery().getTestcase(), data.getSkip(), data.getLimit()));

				return new ResponseEntity<Object>(testcaseResponse.getBody(), HttpStatus.OK);

			}
			map.put("status", "success");
			map.put("message", "invalid query");
			return new ResponseEntity<>(map, HttpStatus.OK);
		} else if (headertoken.length() <= 0) {
			map.put("Status", "Failure");
			map.put("message", "token can't be blanked");
			return new ResponseEntity<>(map, HttpStatus.OK);

		} else {

			map.put("Status", "Failure");
			map.put("message", "token is incorrect");
			return new ResponseEntity<>(map, HttpStatus.OK);

		}

	}

}
