package com.mongoDb.connection.mongodbAPI;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mongoDb.connection.baseClass.GetConfig;
import com.mongoDb.connection.mongoimplementation.ProjectImpl;
import com.mongoDb.connection.selenium.SeleniumStart;

@RestController
@RequestMapping("/platformtesting2")
public class CreatingFindAPI {

	@Autowired
	ProjectImpl projectimpl;

	@GetMapping(path = "/{projectId}")
	public Map<String, String> creatingApi(@PathVariable("projectId") String projectId,
			@RequestHeader("token") String headertoken) throws IOException {
		HttpHeaders headers = new HttpHeaders();
		headers.set("token", GetConfig.getConfigure("token"));
		Map<String, String> responsemessage = new HashMap<>();
		boolean empty = projectimpl.findById(projectId).isEmpty();
		if (headertoken.equals(GetConfig.getConfigure("token"))) {

			if (empty) {
				
				responsemessage.put("status", "failure");
				responsemessage.put("message", "Does not contain project for this given project id...");
				responsemessage.put("Chrome", "No active sessions for this credit");
				return responsemessage;

			} else {

				responsemessage.put("status", "success");
				responsemessage.put("message", "contains project for the given projectId....!!");
				String property = System.getProperty("java.home");
				String property2 = System.getProperty("java.class.path");
				String property3 = property + File.separator + "bin" + File.separator + "java";
				ProcessBuilder process = new ProcessBuilder(property3, "-classpath", property2,
						SeleniumStart.class.getName(), projectId);
				process.inheritIO();
				process.start();
				return responsemessage;

			}
		} else if (headertoken.length() <= 0) {
			responsemessage.put("status", "failure");
			responsemessage.put("message", "header value cannot be blank");
			return responsemessage;
		} else {
			responsemessage.put("status", "failure");
			responsemessage.put("message", "header value is wrong...!!");
			return responsemessage;
		}

	}

}
