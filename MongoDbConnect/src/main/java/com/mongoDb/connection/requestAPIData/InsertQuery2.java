package com.mongoDb.connection.requestAPIData;

public class InsertQuery2 {
	private String projectId;
	private String moduleId;
	private String scenarioId;
	private String componentId;
	private String testcaseId;
	private FinalResult finalresult;

	
	public FinalResult getFinalresult() {
		return finalresult;
	}

	public void setFinalresult(FinalResult finalresult) {
		this.finalresult = finalresult;
	}

	public String getProjectId() {
		return projectId;
	}

	public void setProjectId(String projectId) {
		this.projectId = projectId;
	}

	public String getModuleId() {
		return moduleId;
	}

	public void setModuleId(String moduleId) {
		this.moduleId = moduleId;
	}

	public String getScenarioId() {
		return scenarioId;
	}

	public void setScenarioId(String scenarioId) {
		this.scenarioId = scenarioId;
	}

	public String getComponentId() {
		return componentId;
	}

	public void setComponentId(String componentId) {
		this.componentId = componentId;
	}

	public String getTestcaseId() {
		return testcaseId;
	}

	public void setTestcaseId(String testcaseId) {
		this.testcaseId = testcaseId;
	}

	public class FinalResult {
		private String result;

		public String getResult() {
			return result;
		}

		public void setResult(String result) {
			this.result = result;
		}

	}
}
