package com.mongoDb.connection.requestAPIData;

public class UpdateDocument {
	private String sessionStatus;

	public String getSessionStatus() {
		return sessionStatus;
	}

	public void setSessionStatus(String sessionStatus) {
		this.sessionStatus = sessionStatus;
	}

}
