package com.mongoDb.connection.requestAPIData;

public class RequestData {

	private String collection;
	private RequestQuery query;
	private int skip;
	private int limit;
	private UpdateDocument doc;

	public UpdateDocument getDoc() {
		return doc;
	}

	public void setDoc(UpdateDocument doc) {
		this.doc = doc;
	}

	public int getSkip() {
		return skip;
	}

	public void setSkip(int skip) {
		this.skip = skip;
	}

	public int getLimit() {
		return limit;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public RequestQuery getQuery() {
		return query;
	}

	public void setQuery(RequestQuery query) {
		this.query = query;
	}

}
