package com.mongoDb.connection.requestAPIData;

import java.util.List;

public class InsertQuery1 {
	private String collection;

	public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	private List<InsertQuery2> doc;

	public List<InsertQuery2> getDoc() {
		return doc;
	}

	public void setDoc(List<InsertQuery2> doc) {
		this.doc = doc;
	}
	
}
